import './App.css';
import Resultat from './resultat'
import React, { useState, useEffect} from 'react';

const App = () => {
  const [poid, setPoid] = useState()
  const [taille, setTaille] = useState()
  const [resultat, setResultat] = useState("")
  const [status, setStatus] = useState("")

  const handlePoid = (e) => {
    setPoid(e.target.value)
  }
  const handleTaille = (e) => {
    setTaille(e.target.value)
  }

  const calculer = () => {
    let x = (poid / Math.pow(taille, 2)).toFixed(2)
    if(x < 18.5)  setStatus("insuffisance pondérale")
    else if(x > 18.5 && x < 24.9) setStatus("poids normal")
    else if(x > 25 && x < 29.9) setStatus("surpoids")
    else setStatus("obésité")

    setResultat("Le résultat est: "+x)
  }

  return (
  <div className="container mt-5">
    <div class="card mb-3">
      <h3 class="card-header">Calcul d'IMC</h3>
      <div class="card-body">
        <div className={"form-group"}>
          <label>Poid (en Kg)</label>
          <input type="text" className={"form-control"} placeholder="Entrer le poid" value={ poid }
          onChange={ handlePoid }/>
        </div>
        <div className={"form-group"}>
          <label>Taille (en m)</label>
          <input type="text" className={"form-control"} placeholder="Entrer le poid" value={ taille }
          onChange={ handleTaille }/>
        </div>
          <button className={"btn btn-secondary mr-4"} onClick={()=>{setPoid(0);setTaille(0)}}>Réinitialiser</button>
          <button className={"btn btn-primary"} onClick={calculer}>Calculer</button>
          {resultat && <Resultat res={resultat} status={status} />}
      </div> 
             
    </div>
    </div>)
}

export default App;
